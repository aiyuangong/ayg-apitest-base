import com.ayg.test.apitest.core.ApiEngine;
import com.ayg.test.apitest.core.CaseUpdateDbListener;
import org.testng.TestNG;
import org.testng.collections.Maps;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class APITestRun {
    public static void main(String[] args) {
        Map map = new HashMap();
        map.putAll(null);
        //testForSuit();
    }

    public static void testForClasses(){
        TestNG testNG = new TestNG();
        testNG.setTestClasses(new Class[]{ApiEngine.class});
        testNG.run();
    }

    public static void testForSuit(){
        XmlSuite suite = new XmlSuite();
        suite.setName("test_payment_system");
        XmlTest test = new XmlTest(suite);
        test.setName("test_alipay");
        Map param = Maps.newHashMap();
        param.put("tag","/payment/test");
        param.put("api_type","payment");
        param.put("env","test");
        test.setParameters(param);
        List<XmlClass> classes = new ArrayList<XmlClass>();
        classes.add(new XmlClass("com.ayg.test.apitest.core.ApiEngine"));
        test.setXmlClasses(classes) ;
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        CaseUpdateDbListener listener = new CaseUpdateDbListener();
        TestNG tng = new TestNG();
        tng.addListener(listener);
        tng.setXmlSuites(suites);
        tng.run();
    }

}
