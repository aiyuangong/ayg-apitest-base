package com.ayg.test.apitest.dao;

import com.ayg.test.apitest.entity.PayTestCase;
import com.ayg.test.apitest.utils.DataSourceByC3p0;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PaymentDao extends BaseDao{

    /**
     * 查询支付系统的测试用例
     * @param sql
     * @param params
     * @return
     */
    public List<PayTestCase> queryPayTestCase(String sql, Object... params){
        try {
            return DataSourceByC3p0.getQueryRunner().query(sql,new BeanListHandler<PayTestCase>(PayTestCase.class),params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
