package com.ayg.test.apitest.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrepareDataDao extends BaseDao{

    public Map<String,String> queryPrepareData(String env,String sql){
        Map<String,String> sqlResult = new HashMap<>();
        String[] fields = sql.substring(sql.lastIndexOf("select")+6,sql.indexOf("from")).trim().split(",");
        List result = super.query(env,sql);
        for(int j=0;j<result.size();j++){
            Object[] objarray = (Object[])result.get(j);
            //Object[] objarray = (Object[])item;
            for(int i=0;i<fields.length;i++){
                String value = "";
                if(objarray==null || objarray.length==0){
                    continue;
                }
                if(objarray[i] instanceof BigDecimal){
                    value =  objarray[i].toString();
                }else if(objarray[i] instanceof Integer){
                    value =  String.valueOf(objarray[i]);
                }else if(objarray[i] instanceof Float){
                    value =  String.valueOf(objarray[i]);
                }else if(objarray[i] instanceof Double){
                    value = String.valueOf(objarray[i]);
                }else if(objarray[i] instanceof Timestamp){
                    value = objarray[i].toString();
                }else {
                    value = (String) objarray[i];
                }
                sqlResult.put(fields[i]+"["+j+"]",value);
            }
        }
        return sqlResult;
    }
}
