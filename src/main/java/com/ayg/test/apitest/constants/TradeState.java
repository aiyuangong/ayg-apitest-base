package com.ayg.test.apitest.constants;

public class TradeState {
    public static final String PROCESSING = "PROCESSING";
    public static final String SUSPEND = "SUSPEND";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";

}
