package com.ayg.test.apitest.constants;

public class RequestConstants {
    private RequestConstants(){}
    public static final String GET = "get";
    public static final String POST = "post";
    public static final String COOKIES = "cookies";
}
