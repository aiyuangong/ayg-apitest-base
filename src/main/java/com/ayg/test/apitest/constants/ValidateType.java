package com.ayg.test.apitest.constants;

public class ValidateType {
    public static final String EQ = "eq";
    public static final String LEN_EQ = "len_eq";
    public static final String NUM_EQ = "num_eq";
    public static final String IS = "is";
    public static final String LT = "lt";
    public static final String LE = "le";
    public static final String GT = "gt";
    public static final String GE = "ge";
    public static final String NE = "ne";
    public static final String HAS = "has";
    public static final String REGEX = "regex";
    public static final String DB = "db";
    public static final String STATUS_CODE = "status_code";
    public static final String SCHEMA = "schema";
}
