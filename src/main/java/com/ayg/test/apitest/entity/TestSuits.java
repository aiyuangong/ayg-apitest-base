package com.ayg.test.apitest.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestSuits {
    private TestCase config;
    private Map<String,String> common = new HashMap<String,String>();
    private List<TestCase> testCaseList = new ArrayList<>();

    public TestCase getConfig() {
        return config;
    }

    public void setConfig(TestCase config) {
        this.config = config;
    }

    public List<TestCase> getTestCaseList() {
        return testCaseList;
    }

    public void setTestCaseList(List<TestCase> testCaseList) {
        this.testCaseList = testCaseList;
    }

    public Map<String, String> getCommon() {
        return common;
    }

    public void setCommon(Map<String, String> common) {
        this.common = common;
    }
}
