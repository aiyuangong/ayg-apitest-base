package com.ayg.test.apitest.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Request implements Serializable {
    private static final long serialVersionUID = 2631590509760908280L;
    private String baseUrl;
    private String url;
    private String method;
    private String paramType = "body";//请求参数默认提交方式
    private Map headers = new HashMap();
    private Map json = new HashMap();
    private Map cookies = new HashMap();
    private String data;
    private String validateData;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map getHeaders() {
        if(headers==null)
            headers = new HashMap();
        return headers;
    }

    public void setHeaders(Map headers) {
        this.headers = headers;
    }

    public Map getJson() {
        return json;
    }

    public void setJson(Map json) {
        this.json = json;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Map getCookies() {
        return cookies;
    }

    public void setCookies(Map cookies) {
        this.cookies = cookies;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getValidateData() {
        return validateData;
    }

    public void setValidateData(String validateData) {
        this.validateData = validateData;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
