package com.ayg.test.apitest.entity;

import java.io.Serializable;

public class Validate implements Serializable {
    private static final long serialVersionUID = 8452545269884521245L;
    private String check;
    private String comparator;
    private String expect;
    private String sql;

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getComparator() {
        return comparator;
    }

    public void setComparator(String comparator) {
        this.comparator = comparator;
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
