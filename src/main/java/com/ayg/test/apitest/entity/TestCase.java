package com.ayg.test.apitest.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestCase implements Serializable {
    private static final long serialVersionUID = 2631590509884521245L;
    private Integer id;
    private String name;
    private Integer type;
    private Request request;
    private List extract;
    private List<Validate> validate;
    private List variables;
    private List beforeAction;
    private List afterAction;
    private String common;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Request getRequest() {
        if(request==null)
            request = new Request();
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public List getExtract() {
        return extract;
    }

    public void setExtract(List extract) {
        this.extract = extract;
    }

    public List<Validate> getValidate() {
        return validate;
    }

    public void setValidate(List validate) {
        this.validate = validate;
    }

    public List getVariables() {
        return variables;
    }

    public void setVariables(List variables) {
        this.variables = variables;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List getBeforeAction() {
        if(beforeAction == null){
            beforeAction = new ArrayList();
        }
        return beforeAction;
    }

    public List getAfterAction() {
        if(afterAction == null){
            afterAction = new ArrayList();
        }
        return afterAction;
    }

    public void setAfterAction(List afterAction) {
        this.afterAction = afterAction;
    }

    public void setBeforeAction(List beforeAction) {
        this.beforeAction = beforeAction;
    }

    public String getCommon() {
        return common;
    }

    public void setCommon(String common) {
        this.common = common;
    }
}