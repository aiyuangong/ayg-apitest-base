package com.ayg.test.apitest.core;

import com.ayg.test.apitest.constants.SqlString;
import com.ayg.test.apitest.dao.PaymentDao;
import com.ayg.test.apitest.entity.PayTestCase;
import java.util.Iterator;
import java.util.List;

public class PaymentDbDataProvider implements Iterator<Object[]> {


    private PaymentDao dao = new PaymentDao();
    Iterator<PayTestCase> payTestCaseIterator;

    public PaymentDbDataProvider(String env){
         List<PayTestCase> dataList = dao.queryPayTestCase(SqlString.PAYMENT_TESTCASE_SQL,env);
         payTestCaseIterator = dataList.iterator();
    }

    @Override
    public boolean hasNext() {
        return  payTestCaseIterator.hasNext();
    }

    @Override
    public Object[] next() {
        PayTestCase testcase = payTestCaseIterator.next();
        Object r[] = new Object[1];
        r[0] = testcase;
        return r;
    }
}
