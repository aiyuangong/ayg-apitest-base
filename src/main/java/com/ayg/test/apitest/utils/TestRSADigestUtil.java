package com.ayg.test.apitest.utils;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import com.ayg.payment.util.IObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class TestRSADigestUtil {
    public static final String UTF8 = "utf-8";
    public static final String SIGN_KEY = "sign";
    private static final String ENCODING = "UTF-8";
    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

    public TestRSADigestUtil() {
    }

    public static String digest(Object obj, String privateKey) {
        try {
            String signData = sign256(toSignStr(obj), privateKey);
            return signData;
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static String toSignStr(Object bean) throws Exception {
        StringBuilder sb = new StringBuilder();
        buildSignStr(bean, sb, 1);
        System.out.println("签名串" + sb.toString());
        return sb.toString();
    }

    private static void buildSignStr(Object bean, StringBuilder sb, int level) {
        if (bean != null) {
            if (bean instanceof IObject) {
                Map<String, Object> map = transBean2Map(bean);
                map2Str(sb, level, map);
            } else if (bean instanceof Iterable) {
                sb.append("[");
                Iterator it = ((Iterable)bean).iterator();

                for(boolean isFirst = true; it.hasNext(); isFirst = false) {
                    if (!isFirst) {
                        sb.append(",");
                    }

                    buildSignStr(it.next(), sb, level + 1);
                }

                sb.append("]");
            }else if(bean instanceof Map){
                Map  map = (Map)bean;
               /* for (Iterator<Map.Entry<String, String>> it = map.entrySet().iterator(); it.hasNext();){
                    Map.Entry<String, String> item = it.next();
                    if(StringUtil.isEmpty(item.getValue())){
                        it.remove();
                    }
                }*/
                for (Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator(); it.hasNext();){
                    Map.Entry<String, Object> item = it.next();
                    if(item.getValue() instanceof String) {
                        if (StringUtil.isEmpty((String)item.getValue()) || "signType".equals(item.getKey())) {
                            it.remove();
                        }
                    }
                }
                map2Str(sb,level,(Map)bean);
            }else{
                sb.append(bean);
            }

        }
    }

    private static void map2Str(StringBuilder sb, int level, Map<String, Object> map) {
        Set<String> keySet = map.keySet();
        Iterator<String> iter = keySet.iterator();
        if (level != 1) {
            sb.append("{");
        }

        for(boolean isFirst = true; iter.hasNext(); isFirst = false) {
            if (!isFirst) {
                sb.append("&");
            }

            String key = (String)iter.next();
            Object value = map.get(key);
            sb.append(key).append("=");
            buildSignStr(value, sb, level + 1);
        }

        if (level != 1) {
            sb.append("}");
        }
    }

    private static Map<String, Object> transBean2Map(Object obj) {
        if (obj == null) {
            return null;
        } else {
            TreeMap map = new TreeMap();

            try {
                BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
                PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
                PropertyDescriptor[] var4 = propertyDescriptors;
                int var5 = propertyDescriptors.length;

                for(int var6 = 0; var6 < var5; ++var6) {
                    PropertyDescriptor property = var4[var6];
                    String key = property.getName();
                    if (!key.equals("class")) {
                        Method getter = property.getReadMethod();
                        Object value = getter.invoke(obj);
                        if (null != value && StringUtils.isNotBlank(String.valueOf(value)) && !key.equalsIgnoreCase("sign")) {
                            map.put(key, value);
                        }
                    }
                }

                return map;
            } catch (Exception var11) {
                throw new RuntimeException("bean2Map excption!");
            }
        }
    }

    private static String sign256(String data, String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, IOException {
        System.out.println("签名字符串" + data);
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(getPrivateKey(privateKey));
        signature.update(data.getBytes("UTF-8"));
        String ret = Base64.encodeBase64String(signature.sign());
        System.out.println("签名" + ret);
        return ret;
    }

    private static boolean verify256(String data, String sign, String publicKey) {
        if (data != null && sign != null && publicKey != null) {
            try {
                Signature signetcheck = Signature.getInstance("SHA256withRSA");
                signetcheck.initVerify(getPublicKey(publicKey));
                signetcheck.update(data.getBytes("UTF-8"));
                return signetcheck.verify(Base64.decodeBase64(sign.getBytes("UTF-8")));
            } catch (Exception var4) {
                return false;
            }
        } else {
            return false;
        }
    }

    private static PublicKey getPublicKey(String key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    private static PrivateKey getPrivateKey(String key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }

    private static String getKeyString(Key key) throws Exception {
        byte[] keyBytes = key.getEncoded();
        String s = (new BASE64Encoder()).encode(keyBytes);
        return s;
    }
}
