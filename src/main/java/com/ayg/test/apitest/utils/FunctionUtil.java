package com.ayg.test.apitest.utils;

import com.ayg.test.apitest.core.ApiEngine;
import com.ayg.test.apitest.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FunctionUtil {
	private static final Logger logger = LoggerFactory.getLogger(FunctionUtil.class);
	private static final Map<String, Class<? extends Function>> functionsMap = new HashMap<String, Class<? extends Function>>();
	static {
		//bodyfile 特殊处理
		//functionsMap.put("bodyfile", null);
		List<Class<?>> clazzes = ClassFinder.getAllAssignedClass(Function.class);
		/*clazzes.forEach((clazz) -> {
			try {
				// function
				Function tempFunc = (Function) clazz.newInstance();
				String referenceKey = tempFunc.getReferenceKey();
				if (referenceKey.length() > 0) { // ignore self
					functionsMap.put(referenceKey, tempFunc.getClass());
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				//TODO 
			}
		});*/
		for(Class clazz: clazzes){
			try {
				// function
				Function tempFunc = (Function) clazz.newInstance();
				String referenceKey = tempFunc.getReferenceKey();
				if (referenceKey.length() > 0) { // ignore self
					functionsMap.put(referenceKey, tempFunc.getClass());
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				//TODO
			}
		}
		logger.info("共有 {} 个预处理方法",functionsMap.size());
	}
	
	public static boolean isFunction(String functionName){
		return functionsMap.containsKey(functionName);
	}
	
	public static String getValue(String functionName,String[] args){
		try {
			return functionsMap.get(functionName).newInstance().execute(args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

}

