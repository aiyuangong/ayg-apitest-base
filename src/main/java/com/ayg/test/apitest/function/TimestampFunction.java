package com.ayg.test.apitest.function;
import java.util.Date;

public class TimestampFunction implements Function{

	@Override
	public String execute(String[] args) {
	    return String.format("%s", new Date().getTime());
	}
	
	@Override
	public String getReferenceKey() {
		return "timestamp";
	}


}
