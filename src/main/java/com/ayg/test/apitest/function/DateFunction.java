package com.ayg.test.apitest.function;

import com.ayg.test.apitest.utils.StringUtil;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFunction  implements Function{

	@Override
	public String execute(String[] args) {
		if (args.length == 0 ||StringUtil.isEmpty(args[0])) {
			return getAssignDate("yyyy-MM-dd HH:mm:ss",new Date().getTime());
		}else{
			return args[0]+ String.format("%s", new Date().getTime());
		}
	}

	private String getCurrentDate(String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String str = format.format(new Date());
		return str;
	}

	private String getAssignDate(String pattern,long time) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String str = format.format(time);
		return str;
	}
	
	@Override
	public String getReferenceKey() {
		return "date";
	}

	public static void main(String[] args){
		DateFunction dateFunction = new DateFunction();
		String datestr = dateFunction.execute(new String[]{"yyyy-MM-dd"});
		System.out.println(datestr);

		// 精确到毫秒
		// 获取当前时间戳
		System.out.println(System.currentTimeMillis());
		System.out.println(Calendar.getInstance().getTimeInMillis());
		System.out.println(new Date().getTime());
	}

}
