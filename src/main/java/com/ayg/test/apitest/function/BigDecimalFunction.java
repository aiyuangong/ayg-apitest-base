package com.ayg.test.apitest.function;

import com.ayg.test.apitest.utils.RandomUtil;
import com.ayg.test.apitest.utils.StringUtil;

import java.math.BigDecimal;

public class BigDecimalFunction implements Function{

	@Override
	public String execute(String[] args) {
		long param = 0L;
		if (args.length == 0 ||StringUtil.isEmpty(args[0])) {
			return  RandomUtil.getRandom(5, true);

	  	}else {
			param = Long.valueOf(args[0]);
			return BigDecimal.valueOf(param).toString();
		}
	}


	
	@Override
	public String getReferenceKey() {
		return "bigDecimal";
	}

	public static void main(String[] args){
		BigDecimalFunction dateFunction = new BigDecimalFunction();
		String datestr = dateFunction.execute(new String[]{});
		System.out.println(datestr);
	}
}
